/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.company.databaseproject.testReport;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kunpo
 */
public class ProfitReportModel {

    private String Date;
    private float totalIncome;
    private float totalExpense;
    private float Profit;

    public ProfitReportModel(String Date, float totalIncome, float totalExpense, float Profit) {
        this.Date = Date;
        this.totalIncome = totalIncome;
        this.totalExpense = totalExpense;
        this.Profit = Profit;
    }

    public ProfitReportModel() {
        this.Date = null;
        this.totalIncome = 0;
        this.totalExpense = 0;
        this.Profit = 0;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public float getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(float totalIncome) {
        this.totalIncome = totalIncome;
    }

    public float getTotalExpense() {
        return totalExpense;
    }

    public void setTotalExpense(float totalExpense) {
        this.totalExpense = totalExpense;
    }

    public float getProfit() {
        return Profit;
    }

    public void setProfit(float Profit) {
        this.Profit = Profit;
    }

    @Override
    public String toString() {
        return "ProfitReport{" + "Date=" + Date + ", totalIncome=" + totalIncome + ", totalExpense=" + totalExpense + ", Profit=" + Profit + '}';
    }

    public static ProfitReportModel fromRS(ResultSet rs) {
        ProfitReportModel obj = new ProfitReportModel();
        try {
            obj.setDate(rs.getString("Month"));
            obj.setTotalIncome(rs.getFloat("total_income"));
            obj.setTotalExpense(rs.getFloat("total_expense"));
            obj.setProfit(rs.getFloat("profit"));

        } catch (SQLException ex) {
            Logger.getLogger(ProfitReportModel.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
