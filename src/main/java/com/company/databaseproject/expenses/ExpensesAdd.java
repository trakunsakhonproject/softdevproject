/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.company.databaseproject.expenses;

import com.mycompany.databaseproject.component.BuyMaterialtable;
import com.mycompany.softdevproject.model.Expenses;
import com.mycompany.softdevproject.model.ExpensesDetail;
import com.mycompany.softdevproject.model.Meterial;
import com.mycompany.softdevproject.service.EmployeeService;
import static com.mycompany.softdevproject.service.EmployeeService.currentUser;
import com.mycompany.softdevproject.service.ExpensesService;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kunpo
 */
public class ExpensesAdd extends javax.swing.JPanel implements BuyMaterialtable {

    private ExpensesService expensesService = new ExpensesService();
    private Expenses expenses;
    private Random random = new Random();

    public ExpensesAdd() {
        initComponents();
        this.expensesService = new ExpensesService();
        expenses = new Expenses();
        txtUserName.setText("User: " + EmployeeService.getCurrentUser().getName());
        expenses.setEmployeeId(EmployeeService.getCurrentUser().getId());
        loadExpensesDetail();
    }

    private void loadExpensesDetail() {
        tblReceiptDetail.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 20));
        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] header = {"Expenses Name", "Expenses Price"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return expenses != null && expenses.getExpensesDetails() != null ? expenses.getExpensesDetails().size() : 0;
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ExpensesDetail> expensesDetails = expenses != null && expenses.getExpensesDetails() != null ? expenses.getExpensesDetails() : new ArrayList<>();
                ExpensesDetail expensesDetail = rowIndex < expensesDetails.size() ? expensesDetails.get(rowIndex) : null;

                if (expensesDetail != null) {
                    switch (columnIndex) {
                        case 0:
                            return expensesDetail.getName();
                        case 1:
                            return expensesDetail.getTotal();
                        default:
                            return "";
                    }
                }
                return "";
            }
        });
    }

    private void refreshExpenseTable() {
        SwingUtilities.invokeLater(() -> {
            AbstractTableModel tableModel = new AbstractTableModel() {
                String[] header = {"Expenses Name", "Expenses Price"};

                @Override
                public String getColumnName(int column) {
                    return header[column];
                }

                @Override
                public int getRowCount() {
                    return expenses != null && expenses.getExpensesDetails() != null ? expenses.getExpensesDetails().size() : 0;
                }

                @Override
                public int getColumnCount() {
                    return 2;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    ArrayList<ExpensesDetail> expensesDetails = expenses != null && expenses.getExpensesDetails() != null ? expenses.getExpensesDetails() : new ArrayList<>();
                    ExpensesDetail expensesDetail = rowIndex < expensesDetails.size() ? expensesDetails.get(rowIndex) : null;

                    if (expensesDetail != null) {
                        switch (columnIndex) {
                            case 0:
                                return expensesDetail.getName();
                            case 1:
                                return expensesDetail.getTotal();
                            default:
                                return "";
                        }
                    }
                    return "";
                }
            };

            tblReceiptDetail.setModel(tableModel);
            tblReceiptDetail.revalidate();
            tblReceiptDetail.repaint();
        });
    }

    public void clearReceipt() {
        expenses = new Expenses();
        txtUserName.setText("ไม่เป็นสมาชิก");
        expenses.setEmployeeId(EmployeeService.getCurrentUser().getId());
        txtTotalReal.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnWater = new javax.swing.JButton();
        btnElec = new javax.swing.JButton();
        btnMat = new javax.swing.JButton();
        btnSS = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtTotalReal = new javax.swing.JLabel();
        btnSaveBill = new javax.swing.JButton();
        btnClearTable = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txtUserName = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenu3.setText("File");
        jMenuBar1.add(jMenu3);

        jMenu4.setText("Edit");
        jMenuBar1.add(jMenu4);

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        btnWater.setText("Water");
        btnWater.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnWaterActionPerformed(evt);
            }
        });

        btnElec.setText("Electric");
        btnElec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnElecActionPerformed(evt);
            }
        });

        btnMat.setText("Mat");
        btnMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMatActionPerformed(evt);
            }
        });

        btnSS.setText("Employee Salary");
        btnSS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(btnWater, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(btnElec, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addComponent(btnMat, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(btnSS)
                .addGap(70, 70, 70))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnWater, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnElec, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMat, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSS, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(168, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(204, 204, 255));

        tblReceiptDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Price", "Qty", "Total"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel2.setText("ราคารวมสุทธิ");

        txtTotalReal.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        txtTotalReal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotalReal.setText("00.00");

        btnSaveBill.setBackground(new java.awt.Color(204, 255, 204));
        btnSaveBill.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btnSaveBill.setText("บันทึกค่าใช้จ่าย");
        btnSaveBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveBillActionPerformed(evt);
            }
        });

        btnClearTable.setBackground(new java.awt.Color(255, 153, 153));
        btnClearTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnClearTable.setText("ยกเลิก");
        btnClearTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearTableActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTotalReal))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClearTable, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(btnSaveBill)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtTotalReal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSaveBill, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClearTable, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE))
                .addContainerGap(35, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(204, 204, 255));

        txtUserName.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        txtUserName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtUserName.setText("UserName:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel6.setText("นำเข้ารายจ่าย");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtUserName))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 423, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    public void refreshExpenseItem() {
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
    }
    private void btnSaveBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveBillActionPerformed
        try {
            if (expenses.getExpensesDetails().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Please select an item.");
            } else {
                JDialog dialog = (JDialog) SwingUtilities.getWindowAncestor(this);
                int userId = currentUser.getId();
                expenses.setId(EmployeeService.getCurrentUser().getId());
                Expenses ex = expensesService.addNew(expenses, userId);
                System.out.println(ex);
                dialog.dispose();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Empty");
        }


    }//GEN-LAST:event_btnSaveBillActionPerformed

    private void btnClearTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearTableActionPerformed
        expenses.getExpensesDetails().clear();
        btnWater.setEnabled(true);
        btnElec.setEnabled(true);
        btnMat.setEnabled(true);
        btnSS.setEnabled(true);
        refreshExpenseTable();
        txtTotalReal.setText("");
        elecTotal = 0;
    }//GEN-LAST:event_btnClearTableActionPerformed

    private void btnWaterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnWaterActionPerformed
        float total = 3000 + random.nextFloat() * (4000 - 3000);
        updateExpenses("ค่าน้ำ", total);
        btnWater.setEnabled(false);
    }//GEN-LAST:event_btnWaterActionPerformed
    private float elecTotal = 0;
    private void btnElecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnElecActionPerformed
        if (elecTotal == 0) {
            elecTotal = 4000 + random.nextFloat() * (5000 - 4000);
        }
        updateExpenses("ค่าไฟ", elecTotal);
        btnElec.setEnabled(false);
    }//GEN-LAST:event_btnElecActionPerformed

    private void btnMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMatActionPerformed
        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new AddExpensesMatPanel(this, expenses, TOOL_TIP_TEXT_KEY));
        dialog.setSize(490, 179);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        dialog.addWindowListener(new WindowAdapter() {
            private List<Expenses> list;

            @Override
            public void windowClosed(WindowEvent e) {
                list = expensesService.getExpenses();
                loadExpensesDetail();
                btnMat.setEnabled(false);
            }
        });
        ExpensesAdd expenses = new ExpensesAdd();
        expenses.calculateTotalExpenses();
        
//        float total = random.nextFloat() * 1000;
//        updateExpenses("ค่าวัตถุดิบ", total);
//        btnMat.setEnabled(false);
    }//GEN-LAST:event_btnMatActionPerformed

    private void btnSSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSSActionPerformed
        // TODO add your handling code here:
        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new AddExpensesSSPanel(this, expenses, TOOL_TIP_TEXT_KEY));
        dialog.setSize(450, 205);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        dialog.addWindowListener(new WindowAdapter() {
            private List<Expenses> list;

            @Override
            public void windowClosed(WindowEvent e) {
                list = expensesService.getExpenses();
                loadExpensesDetail();
                btnSS.setEnabled(false);
            }
        });
        
    }//GEN-LAST:event_btnSSActionPerformed

    public void calculateTotalExpenses() {
        float totalWater = 0;
        float totalElectricity = 0;
        float totalMaterials = 0;
        float totalSummarySalary = 0;

        if (expenses != null && expenses.getExpensesDetails() != null) {
            for (ExpensesDetail expensesDetail : expenses.getExpensesDetails()) {
                if ("ค่าน้ำ".equals(expensesDetail.getName())) {
                    totalWater += expensesDetail.getTotal();
                } else if ("ค่าไฟ".equals(expensesDetail.getName())) {
                    totalElectricity += expensesDetail.getTotal();
                } else if ("ค่าวัตถุดิบ".equals(expensesDetail.getName())) {
                    totalMaterials += expensesDetail.getTotal();
                } else if ("รวมรายจ่ายพนักงาน".equals(expensesDetail.getName())) {
                    totalSummarySalary += expensesDetail.getTotal();
                }
            }
        }

        float totalReal = totalWater + totalElectricity + totalMaterials + totalSummarySalary;
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        String formattedTotal = decimalFormat.format(totalReal);
        txtTotalReal.setText(formattedTotal);
    }

    private void updateExpenses(String expenseName, float total) {
        boolean checkExpenses = false;
        if (expenses != null && expenses.getExpensesDetails() != null) {
            for (ExpensesDetail expensesDetail : expenses.getExpensesDetails()) {
                if (expenseName.equals(expensesDetail.getName())) {
                    expensesDetail.setTotal(expensesDetail.getTotal() + total);
                    expenses.calculateTotal();
                    checkExpenses = true;
                    System.out.println(expensesDetail.getName());
                    break;
                }
            }
            if (!checkExpenses) {
                expenses.addExpensesDetail(expenseName, total);
            }
        }
        calculateTotalExpenses();
        refreshExpenseTable();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearTable;
    private javax.swing.JButton btnElec;
    private javax.swing.JButton btnMat;
    private javax.swing.JButton btnSS;
    private javax.swing.JButton btnSaveBill;
    private javax.swing.JButton btnWater;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JLabel txtTotalReal;
    private javax.swing.JLabel txtUserName;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Meterial material, int qty, float price) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
