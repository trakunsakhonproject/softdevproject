/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.company.databaseproject.posBill;

import com.company.databaseproject.posReceipt.*;
import com.company.databaseproject.employees.EmployeeAddNewPanel;
import com.company.databaseproject.member.MemberAddNewPanel;
import com.mycompany.databaseproject.component.BuyMaterialtable;
import com.mycompany.databaseproject.component.BuyProductable;
import com.mycompany.databaseproject.component.MaterialListPanel;
import com.mycompany.databaseproject.component.ProductListPanel;
import com.mycompany.softdevproject.model.Bill;
import com.mycompany.softdevproject.model.BillDetail;
import com.mycompany.softdevproject.model.Member;
import com.mycompany.softdevproject.model.Meterial;
import com.mycompany.softdevproject.model.Product;
import com.mycompany.softdevproject.model.Receipt;
import com.mycompany.softdevproject.model.ReceiptDetail;
import com.mycompany.softdevproject.model.Supplier;
import com.mycompany.softdevproject.service.BillService;
import com.mycompany.softdevproject.service.EmployeeService;
import static com.mycompany.softdevproject.service.EmployeeService.currentUser;
import com.mycompany.softdevproject.service.MemberService;
import com.mycompany.softdevproject.service.MeterialService;
import com.mycompany.softdevproject.service.ProductService;
import com.mycompany.softdevproject.service.ReceiptService;
import com.mycompany.softdevproject.service.SupplierService;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author kunpo
 */
public class BillOfSale extends javax.swing.JPanel implements BuyMaterialtable {

    ArrayList<Meterial> materials;
    private BillService billService;
    MeterialService materialService = new MeterialService();
    Meterial material;
//    BillService billService = new BillService();
    private Supplier editedMember;
    private final MaterialListPanel materialListPanel;
    private SupplierService supplierService;
    private List<Supplier> list;
    private Bill bill;
    private Supplier editedSupplier;
    private float discount;

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    /**
     * Creates new form PointOfSale
     */
    public BillOfSale() {
        initComponents();
        Datetime();
        this.billService = new BillService();
        bill = new Bill();
        txtUserName.setText("User: " + EmployeeService.getCurrentUser().getName());
        bill.setUserId(EmployeeService.getCurrentUser().getId());
        tblReceiptDetail.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 20));
        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return bill.getBillDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return billDetail.getMaterialName();
                    case 1:
                        return billDetail.getMaterialPrice();
                    case 2:
                        return billDetail.getAmount();
                    case 3:
                        return billDetail.getTotal();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    billDetail.setAmount(qty);
                    bill.calculateTotal();
                    refreshBill();
                }
                if (columnIndex == 1) {
                    int materialPrice = Integer.parseInt((String) aValue);
                    if (materialPrice < 1) {
                        return;
                    }
                    billDetail.setMaterialPrice(materialPrice);
                    bill.calculateTotal();
                    refreshBill();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return true;
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }
        });
        materialListPanel = new MaterialListPanel();
        materialListPanel.addOnBuyMaterial(this);
        scrMaterialList.setViewportView(materialListPanel);

    }

    private void refreshBill() {
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
        txtsubTotal.setText("" + bill.getSubTotal());
        txtTotalReal.setText("" + bill.getTotal());

    }

    public void clearReceipt() {
        bill = new Bill();
        lblmemberPos.setText("ไม่เป็นสมาชิก");
        bill.setUserId(EmployeeService.getCurrentUser().getId());
        txtDiscount.setText("");
        refreshBill();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        scrMaterialList = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtsubTotal = new javax.swing.JLabel();
        txtTotalReal = new javax.swing.JLabel();
        btnAddSupplier = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnSaveBill = new javax.swing.JButton();
        lblMenberPos = new javax.swing.JLabel();
        lblmemberPos = new javax.swing.JLabel();
        btnClearTable = new javax.swing.JButton();
        txtDiscount = new javax.swing.JTextField();
        btnCal = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        txtUserName = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenu3.setText("File");
        jMenuBar1.add(jMenu3);

        jMenu4.setText("Edit");
        jMenuBar1.add(jMenu4);

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(204, 255, 255));

        scrMaterialList.setBackground(new java.awt.Color(204, 204, 255));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMaterialList, javax.swing.GroupLayout.PREFERRED_SIZE, 862, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMaterialList, javax.swing.GroupLayout.PREFERRED_SIZE, 804, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("ราคารวม                                                                                    ");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("ส่วนลด                                                                               ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel2.setText("ราคารวมสุทธิ");

        txtsubTotal.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtsubTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtsubTotal.setText("00.00");

        txtTotalReal.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        txtTotalReal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotalReal.setText("00.00");

        btnAddSupplier.setBackground(new java.awt.Color(204, 255, 255));
        btnAddSupplier.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAddSupplier.setText("สมัครผู้จัดจําหน่าย");
        btnAddSupplier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddSupplierActionPerformed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(204, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSearch.setText("ค้นหาผู้จัดจําหน่าย");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnSaveBill.setBackground(new java.awt.Color(0, 204, 204));
        btnSaveBill.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btnSaveBill.setText("พิมพ์ใบนําเข้ารายการสินค้า");
        btnSaveBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveBillActionPerformed(evt);
            }
        });

        lblMenberPos.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        lblMenberPos.setText("ผู้จัดจําหน่าย");

        lblmemberPos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblmemberPos.setText("ไม่มีผู้จัดจําหน่าย");

        btnClearTable.setBackground(new java.awt.Color(255, 204, 204));
        btnClearTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnClearTable.setText("ยกเลิกรายการสินค้า");
        btnClearTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearTableActionPerformed(evt);
            }
        });

        txtDiscount.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        btnCal.setBackground(new java.awt.Color(255, 255, 204));
        btnCal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCal.setText("คํานวณ");
        btnCal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 572, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTotalReal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtsubTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(lblMenberPos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblmemberPos, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClearTable)
                        .addGap(18, 18, 18)
                        .addComponent(btnSaveBill))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCal)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAddSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMenberPos)
                        .addComponent(lblmemberPos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtsubTotal)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnCal, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(11, 11, 11)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotalReal)
                            .addComponent(jLabel2))))
                .addGap(40, 40, 40)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSaveBill, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClearTable, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );

        tblReceiptDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Price", "Qty", "Total"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 204, 255));

        txtUserName.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtUserName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtUserName.setText("UserName:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel6.setText("นําเข้ารายการสินค้า");

        lblTime.setText("jLabel3");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel6)
                .addGap(65, 65, 65)
                .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(txtUserName))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTime, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SearchSupplierDialog searchSupplierDialog = new SearchSupplierDialog(this, bill);
        searchSupplierDialog.setLocationRelativeTo(null);
        searchSupplierDialog.setVisible(true);
        searchSupplierDialog.setModal(true);
        searchSupplierDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (bill.getSupplier() != null) {
                    lblmemberPos.setText(bill.getSupplier().getName());
                }
            }
        });
    }//GEN-LAST:event_btnSearchActionPerformed
    private void setObjectToForm() {
        editedSupplier.setName(editedSupplier.getName());
    }
    private void btnAddSupplierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddSupplierActionPerformed
        editedSupplier = new Supplier();
        setObjectToForm();

        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new MemberAddNewPanel());
        dialog.setSize(550, 250);
        dialog.setLocationRelativeTo(null);
        dialog.setModal(true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnAddSupplierActionPerformed

    private void btnSaveBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveBillActionPerformed
        try {
            if (bill.getBillDetails().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Unable to pay", "Please select the product first. ",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                JDialog dialog = (JDialog) SwingUtilities.getWindowAncestor(this);
                int userId = currentUser.getId();
                Bill currentBill = billService.addNew(bill, userId);
                updateExistingMaterials(currentBill);
                clearReceipt();
                dialog.dispose();
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "An error occurred while saving data: " + e.getMessage(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSaveBillActionPerformed
    private void updateExistingMaterials(Bill currentBill) {
        for (BillDetail billDetail : currentBill.getBillDetails()) {
            Meterial material = materialService.getId(billDetail.getMaterialId());
            material.setBalance(material.getBalance() + billDetail.getAmount());
            materialService.update(material);
        }
    }
    private void btnClearTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearTableActionPerformed
        bill.delBillDetail();
        txtDiscount.setText("");
        btnClearTable.setVisible(false);
        refreshBill();

    }//GEN-LAST:event_btnClearTableActionPerformed

    private void btnCalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalActionPerformed
        if (Float.parseFloat(txtDiscount.getText()) < 0 || Float.parseFloat(txtDiscount.getText()) > bill.getSubTotal()) {
            JOptionPane.showConfirmDialog(this, "กรุณาใส่จำนวนเงินให้ถูกต้อง", "ไม่สามารถชำระเงินได้ ",
                    JOptionPane.ERROR_MESSAGE, JOptionPane.ERROR_MESSAGE);
        } else {
            float discount = Float.parseFloat(txtDiscount.getText());
            bill.setDiscount(discount);
            bill.setTotal(bill.getSubTotal() - bill.getDiscount());
            refreshBill();
        }
    }//GEN-LAST:event_btnCalActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddSupplier;
    private javax.swing.JButton btnCal;
    private javax.swing.JButton btnClearTable;
    private javax.swing.JButton btnSaveBill;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMenberPos;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblmemberPos;
    private javax.swing.JScrollPane scrMaterialList;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JLabel txtTotalReal;
    private javax.swing.JLabel txtUserName;
    private javax.swing.JLabel txtsubTotal;
    // End of variables declaration//GEN-END:variables

//    private void setObjectToForm() {
//        editedMember.setName(editedMember.getName());
//        editedMember.setTel(editedMember.getTel());
//        editedMember.setPoint(editedMember.getPoint());
//    }}
    @Override
    public void buy(Meterial material, int qty, float price) {
        boolean checkProduct = false;
        for (BillDetail billDetail : bill.getBillDetails()) {
            if (billDetail.getMaterialName().equals(material.getName())) {
                billDetail.setAmount(billDetail.getAmount() + qty);
                bill.calculateTotal();
                checkProduct = true;
                break;
            }
        }
        if (!checkProduct) {
            bill.addBillDetail(material, qty, price);
            btnClearTable.setVisible(true);
        }
        refreshBill();
    }

    private void Datetime() {
        ActionListener updateDateTimeAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalDateTime dateTime = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                String formattedDateTime = dateTime.format(formatter);
                lblTime.setText("Date and Time: " + formattedDateTime);
            }
        };
        Timer timer = new Timer(1000, updateDateTimeAction);
        timer.setInitialDelay(0);
        timer.start();

    }
}
