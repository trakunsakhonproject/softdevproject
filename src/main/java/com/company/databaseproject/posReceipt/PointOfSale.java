/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.company.databaseproject.posReceipt;

import com.company.databaseproject.employees.EmployeeAddNewPanel;
import com.company.databaseproject.member.MemberAddNewPanel;
import com.mycompany.databaseproject.component.BuyProductable;
import com.mycompany.databaseproject.component.ProBakeryListPanel;
import com.mycompany.databaseproject.component.ProductListPanel;
import com.mycompany.softdevproject.model.Member;
import com.mycompany.softdevproject.model.Product;
import com.mycompany.softdevproject.model.Receipt;
import com.mycompany.softdevproject.model.ReceiptDetail;
import com.mycompany.softdevproject.service.EmployeeService;
import static com.mycompany.softdevproject.service.EmployeeService.currentUser;
import com.mycompany.softdevproject.service.MemberService;
import com.mycompany.softdevproject.service.ProductService;
import com.mycompany.softdevproject.service.ReceiptService;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.swing.Timer;

/**
 *
 * @author kunpo
 */
public class PointOfSale extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    private ReceiptService receiptService;
    ProductService productService = new ProductService();
    Receipt receipt;
    ReceiptService receiptservice = new ReceiptService();
    private Member editedMember;
    private final ProductListPanel productListPanel;
    private JDialog paymentDialog;
    private MemberService memberService;
    private List<Member> list;
    private final ProBakeryListPanel proBakeryListPanel;

    public void setReceipt(Receipt receip) {
        this.receipt = receipt;
    }

    /**
     * Creates new form PointOfSale
     */
    public PointOfSale() {
        initComponents();
        Datetime();
        this.receiptService = new ReceiptService();
        receipt = new Receipt();
        txtUserName.setText("User: " + EmployeeService.getCurrentUser().getName());
        receipt.setUserId(EmployeeService.getCurrentUser().getId());
        tblReceiptDetail.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 20));
        tblReceiptDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getProductName();
                    case 1:
                        return recieptDetail.getProductPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return recieptDetail.getTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    receiptDetail.setQty(qty);
                    receipt.calculateTotal();
                    refreshReceipt();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }
        });
        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);

        proBakeryListPanel = new ProBakeryListPanel();
        proBakeryListPanel.addOnBuyProduct(this);
        scrProductBakeryList.setViewportView(proBakeryListPanel);
        txtsubTotal.setText("00.00");
        txtTotalReal.setText("00.00");
        lblDiscount.setText("00.00");
        btnUsePoint.setVisible(false);
        btnClearTable.setVisible(false);

    }

    private void refreshReceipt() {
        tblReceiptDetail.revalidate();
        tblReceiptDetail.repaint();
        txtsubTotal.setText("" + receipt.getSubtotal());
        txtTotalReal.setText("" + receipt.getTotal());
        lblDiscount.setText("" + receipt.getDiscount());
        btnUsePoint.setVisible(false);

    }

    public void clearReceipt() {
        receipt = new Receipt();
        lblmemberPos.setText("ไม่เป็นสมาชิก");
        lblusePoint.setText("0");
        receipt.setUserId(EmployeeService.getCurrentUser().getId());
        refreshReceipt();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu3 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scrProductList = new javax.swing.JScrollPane();
        scrProductBakeryList = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtsubTotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        txtTotalReal = new javax.swing.JLabel();
        btnAddMember = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        btnPayment = new javax.swing.JButton();
        lblMenberPos = new javax.swing.JLabel();
        lblmemberPos = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        btnClearTable = new javax.swing.JButton();
        btnUsePoint = new javax.swing.JButton();
        txtusePoint = new javax.swing.JLabel();
        lblusePoint = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblReceiptDetail = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        txtUserName = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();

        jMenu1.setText("jMenu1");

        jMenu2.setText("jMenu2");

        jMenu3.setText("File");
        jMenuBar1.add(jMenu3);

        jMenu4.setText("Edit");
        jMenuBar1.add(jMenu4);

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 204, 204));

        jPanel2.setBackground(new java.awt.Color(204, 255, 255));

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        scrProductList.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jTabbedPane1.addTab("เครื่องดื่ม", scrProductList);

        scrProductBakeryList.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jTabbedPane1.addTab("ของหวาน/ทานเล่น", scrProductBakeryList);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 915, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 901, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 858, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 845, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("เครื่องดื่ม\nของหวาน");

        jPanel4.setBackground(new java.awt.Color(204, 204, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("ราคารวม                                                                                    ");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("ส่วนลด                                                                               ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        jLabel2.setText("ราคารวมสุทธิ");

        txtsubTotal.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtsubTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtsubTotal.setText("00.00");

        lblDiscount.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("00.00");

        txtTotalReal.setFont(new java.awt.Font("Tahoma", 0, 28)); // NOI18N
        txtTotalReal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtTotalReal.setText("00.00");

        btnAddMember.setBackground(new java.awt.Color(204, 255, 255));
        btnAddMember.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnAddMember.setText("สมัครสมาชิก");
        btnAddMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddMemberActionPerformed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(204, 255, 255));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnSearch.setText("ค้นหาสมาชิก");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnPayment.setBackground(new java.awt.Color(0, 204, 204));
        btnPayment.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPayment.setText("Payment");
        btnPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentActionPerformed(evt);
            }
        });

        lblMenberPos.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        lblMenberPos.setText("สมาชิก :");

        lblmemberPos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblmemberPos.setText("ไม่เป็นสมาชิก");

        jLabel5.setText("------------------------------------------------------------------------------------------------------------------------------------------------------------");

        btnClearTable.setBackground(new java.awt.Color(255, 204, 204));
        btnClearTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClearTable.setText("ยกเลิกรายการ");
        btnClearTable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearTableActionPerformed(evt);
            }
        });

        btnUsePoint.setBackground(new java.awt.Color(204, 255, 204));
        btnUsePoint.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnUsePoint.setText("ใช้เเต้มสะสมสมาชิก");
        btnUsePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsePointActionPerformed(evt);
            }
        });

        txtusePoint.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        txtusePoint.setText("เเต้มสมาชิกคงเหลือ :");

        lblusePoint.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblusePoint.setText("0");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 545, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalReal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtsubTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnClearTable)
                        .addGap(18, 18, 18)
                        .addComponent(btnPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(lblMenberPos)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblmemberPos, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtusePoint)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblusePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnUsePoint, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnAddMember, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAddMember, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMenberPos)
                        .addComponent(lblmemberPos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtusePoint)
                        .addComponent(lblusePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(txtsubTotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDiscount)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotalReal)
                            .addComponent(jLabel2))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnClearTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPayment, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(204, 255, 255));

        tblReceiptDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Name", "Price", "Qty", "Total"
            }
        ));
        jScrollPane1.setViewportView(tblReceiptDetail);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(204, 204, 255));

        txtUserName.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        txtUserName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtUserName.setText("UserName:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jLabel6.setText("D-Coffee");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(jLabel6))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(txtUserName)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        SearchMemberDialog searchMemberDialog = new SearchMemberDialog(this, receipt);
        searchMemberDialog.setLocationRelativeTo(null);
        searchMemberDialog.setVisible(true);
        searchMemberDialog.setModal(true);
        searchMemberDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (receipt.getCustomer() != null) {
                    lblmemberPos.setText(receipt.getCustomer().getName());
                    lblusePoint.setText("" + receipt.getCustomer().getPoint());
                    btnUsePoint.setVisible(true);
                }
            }
        });
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnAddMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddMemberActionPerformed
        editedMember = new Member();
        setObjectToForm();

        JDialog dialog = new JDialog();
        dialog.getContentPane().add(new MemberAddNewPanel());
        dialog.setSize(520, 220);
        dialog.setLocationRelativeTo(null);
        dialog.setModal(true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnAddMemberActionPerformed

    private void btnPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentActionPerformed
        ArrayList<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();
        if (receiptDetails != null && !receiptDetails.isEmpty()) {
            openPayment();
            btnClearTable.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(this, "Please add a menu before proceeding to payment.");
        }
    }//GEN-LAST:event_btnPaymentActionPerformed
    private void openPaymentCashtDialog() {
        PaymentCashDialog paymentCashDialog = new PaymentCashDialog(this, receipt);
        paymentCashDialog.setLocationRelativeTo(this);
        paymentCashDialog.setVisible(true);
        paymentCashDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {

            }
        });
    }

    private void openPayment() {
        PaymentDialog paymentDialog = new PaymentDialog(this, receipt);
        paymentDialog.setLocationRelativeTo(this);
        paymentDialog.setVisible(true);
        paymentDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                int userId = currentUser.getId();
                if (receipt.getPayment().equals("เงินสด")) {
                    openPaymentCashtDialog();
                } else if (receipt.getPayment().equals("โอนจ่าย")) {
                    System.out.println("" + receipt);
                    Receipt currentReceipt = receiptservice.addNew(receipt, userId);
                    clearReceipt();
                    showBill(currentReceipt);

                } else if (receipt.getPayment().equals("บัตรเครดิต")) {
                    System.out.println("" + receipt);
                    Receipt currentReceipt = receiptservice.addNew(receipt, userId);
                    clearReceipt();
                    showBill(currentReceipt);
                } else {

                }
            }
        });
    }

    public void showBill(Receipt currentReciept) {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        ReceiptDialogForm recieptDialog = new ReceiptDialogForm(frame, currentReciept);
        recieptDialog.setLocationRelativeTo(this);
        recieptDialog.setVisible(true);
    }

    public void closePaymentDialog() {
        if (paymentDialog != null && paymentDialog.isShowing()) {
            paymentDialog.dispose();
        }
    }
    private void btnClearTableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearTableActionPerformed
        receipt.delReceiptDetail();
        btnClearTable.setVisible(false);
        refreshReceipt();
    }//GEN-LAST:event_btnClearTableActionPerformed

    private void btnUsePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsePointActionPerformed

        UsePointDialog usepointDialog = new UsePointDialog(this, receipt);
        usepointDialog.setLocationRelativeTo(null);
        usepointDialog.setVisible(true);
        usepointDialog.setModal(true);
        usepointDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (receipt.getCustomer() != null) {
                    lblusePoint.setText("" + receipt.getCustomer().getPoint());
                    txtsubTotal.setText("" + receipt.getSubtotal());
                    lblDiscount.setText("" + receipt.getDiscount());
                    txtTotalReal.setText("" + receipt.getTotal());
                }
            }
        });
    }//GEN-LAST:event_btnUsePointActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddMember;
    private javax.swing.JButton btnClearTable;
    private javax.swing.JButton btnPayment;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUsePoint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMenberPos;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblmemberPos;
    private javax.swing.JLabel lblusePoint;
    private javax.swing.JScrollPane scrProductBakeryList;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblReceiptDetail;
    private javax.swing.JLabel txtTotalReal;
    private javax.swing.JLabel txtUserName;
    private javax.swing.JLabel txtsubTotal;
    private javax.swing.JLabel txtusePoint;
    // End of variables declaration//GEN-END:variables

    private void setObjectToForm() {
        editedMember.setName(editedMember.getName());
        editedMember.setTel(editedMember.getTel());
        editedMember.setPoint(editedMember.getPoint());
    }

    @Override
    public void buy(Product product, String productName, String size, String type, String sweet, float price, int qty) {
        btnClearTable.setVisible(true);
        boolean checkProduct = false;
        for (ReceiptDetail receiptDetail : receipt.getReceiptDetails()) {
            if (receiptDetail.getProductName().equals(product.getName())) {
                receiptDetail.setQty(receiptDetail.getQty() + qty);
                receipt.calculateTotal();
                checkProduct = true;
                break;
            }
        }
        if (!checkProduct) {
            receipt.addReceiptDetail(product, productName, size, type, sweet, price, qty);
        }
        refreshReceipt();
    }

    private void Datetime() {
        ActionListener updateDateTimeAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LocalDateTime dateTime = LocalDateTime.now();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                String formattedDateTime = dateTime.format(formatter);
                lblTime.setText("Date and Time: " + formattedDateTime);
            }
        };
        Timer timer = new Timer(1000, updateDateTimeAction);
        timer.setInitialDelay(0);
        timer.start();

    }
}
