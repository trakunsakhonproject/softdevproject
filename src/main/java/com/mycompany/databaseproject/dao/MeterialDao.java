/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.Meterial;
import com.mycompany.softdevproject.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public class MeterialDao implements Dao<Meterial> {

    @Override
    public Meterial get(int id) {
         Meterial meterial = null;
        String sql = "SELECT * FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                meterial = Meterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return meterial;

    }

    @Override
    public List<Meterial> getAll() {
        ArrayList<Meterial> list = new ArrayList();
        String sql = "SELECT * FROM material ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Meterial meterial = Meterial.fromRS(rs);
                list.add(meterial);
                
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Meterial> getAll(String order) {
        ArrayList<Meterial> list = new ArrayList();
        String sql = "SELECT * FROM material ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Meterial material = Meterial.fromRS(rs);
                list.add(material);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Meterial save(Meterial obj) {
        try {
            String sql = "INSERT INTO material (mat_name, mat_balance, mat_minimum)"
                    + "VALUES(?, ?, ?)";
            Connection conn = DatabaseHelper.getConnect();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getBalance());
            stmt.setInt(3, obj.getMinimum());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
           System.out.println(ex.getMessage());
           return null;
        }
        return obj;
    }

    @Override
    public Meterial update(Meterial obj) {
        try {
            String sql = "UPDATE material"
                    + " SET mat_name = ?, mat_balance= ?, mat_minimum = ?"
                    + " WHERE mat_id = ?";
            Connection conn = DatabaseHelper.getConnect();
            
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getBalance());
            stmt.setInt(3, obj.getMinimum());
            stmt.setInt(4, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
          System.out.println(ex.getMessage());
          return null;
        }
    
        
    }
    @Override 
    public int delete(Meterial obj) {
          String sql = "DELETE FROM material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }

    @Override
    public List<Meterial> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
