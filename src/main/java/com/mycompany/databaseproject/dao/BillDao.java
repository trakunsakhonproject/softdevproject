/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.Bill;
import com.mycompany.softdevproject.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class BillDao implements Dao<Bill> {

    @Override
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = bill.fromRS(rs);
                BillDetailDao rdd = new BillDetailDao();
                ArrayList<BillDetail> billDetail = (ArrayList<BillDetail>) rdd.getAll("bill_id=" + bill.getId(), " bill_detail_id ");
                bill.setBillDetails(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    public int getTotalMonth(int month) {
        int total = 0;
        String sql = "SELECT SUM(bill_total) AS Total FROM bill WHERE strftime('%m', bill_date) = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, String.format("%02d", month)); // Ensuring the month is in two-digit format
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return total;
    }

    public List<BillDetail> getByBillId(int id) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM bill_detail WHERE bill_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
                BillDetailDao rdd = new BillDetailDao();
                ArrayList<BillDetail> billDetail = (ArrayList<BillDetail>) rdd.getAll("bill_id=" + bill.getId(), " bill_detail_id ");
                bill.setBillDetails(billDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Bill> getAllDESC(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);
                BillDetailDao rdd = new BillDetailDao();
                ArrayList<BillDetail> billDetail = (ArrayList<BillDetail>) rdd.getAll("bill_id=" + bill.getId(), " bill_detail_id ");
                bill.setBillDetails(billDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Bill save(Bill obj) {
        String sql = "INSERT INTO bill (bill_total,bill_discount,bill_subTotal,bill_totalQty,employee_id,supplier_id)"
                + "VALUES(?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getSubTotal());
            stmt.setInt(4, obj.getTotalQty());
            stmt.setInt(5, obj.getUserId());
            stmt.setInt(6, obj.getSupplierId());
            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET bill_name = ?,bill_total = ?,bill_discount = ?,bill_subTotal = ?,bill_totalQty = ?,employee_id = ?,bill_supplier_id = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getDiscount());
            stmt.setFloat(3, obj.getSubTotal());
            stmt.setInt(4, obj.getTotalQty());
            stmt.setInt(5, obj.getUserId());
            stmt.setInt(6, obj.getSupplierId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Bill> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Bill> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
