/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.company.databaseproject.testReport.ProfitReportModel;
import com.company.databaseproject.testReport.ProfitReportPanel;
import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.BillDetail;
import com.mycompany.softdevproject.model.Receipt;
import com.mycompany.softdevproject.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id ) {
        Receipt receipt = null;
        String sql = "SELECT * FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails = (ArrayList<ReceiptDetail>) rdd.getAll("receipt_id=" + receipt.getId(), " receipt_detail_id ");
                receipt.setReceiptDetails(receiptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }

    public List<ReceiptDetail> getByReceiptId(int id) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM receipt_detail WHERE receipt_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReceiptDetail receitptDetail = ReceiptDetail.fromRS(rs);
                list.add(receitptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Receipt> getAll(String order) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {

        String sql = "INSERT INTO receipt (receipt_total,receipt_subtotal,receipt_cash,receipt_total_qty,receipt_discount,receipt_payment,receipt_change,employee_id,member_id)"
                + "VALUES(?, ?,?,?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getSubtotal());
            stmt.setFloat(3, obj.getCash());
            stmt.setInt(4, obj.getTotalQty());
            stmt.setFloat(5, obj.getDiscount());
            stmt.setString(6, obj.getPayment());
            stmt.setFloat(7, obj.getChange());
            stmt.setInt(8, obj.getUserId());
            stmt.setInt(9, obj.getCustomerId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE receipt"
                + " SET receipt_total = ?, receipt_subtotal = ?, receipt_cash = ?, receipt_total_qty = ?, receipt_discount = ?, receipt_payment = ? receipt_change = ?, employee_id = ?, member_id = ?"
                + " WHERE receipt_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setFloat(2, obj.getSubtotal());
            stmt.setFloat(3, obj.getCash());
            stmt.setInt(4, obj.getTotalQty());
            stmt.setFloat(5, obj.getDiscount());
            stmt.setString(6, obj.getPayment());
            stmt.setFloat(7, obj.getChange());
            stmt.setInt(8, obj.getUserId());
            stmt.setInt(9, obj.getCustomerId());
            stmt.setInt(10, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<ProfitReportModel> Profit() {
        ArrayList<ProfitReportModel> list = new ArrayList();
        String sql = """
                WITH ExpenseTotal AS (
                     SELECT strftime('%Y-%m', expenses_date) AS Month, COALESCE(SUM(expenses_total), 0) AS total_expense
                     FROM expenses
                     WHERE expenses_date BETWEEN '2023-10-01' AND '2023-12-01'
                     GROUP BY strftime('%Y-%m', expenses_date)
                 ),
                 IncomeTotal AS (
                     SELECT strftime('%Y-%m', receipt_date) AS Month, COALESCE(SUM(receipt_total), 0) AS total_income
                     FROM receipt
                     WHERE receipt_date BETWEEN '2023-10-01' AND '2023-12-01'
                     GROUP BY strftime('%Y-%m', receipt_date)
                 )
                 
                 SELECT Month, SUM(total_income) AS total_income, SUM(total_expense) AS total_expense, SUM(total_income) - SUM(total_expense) AS profit
                 FROM (
                     SELECT Month, total_income, 0 AS total_expense FROM IncomeTotal
                     UNION ALL
                     SELECT Month, 0 AS total_income, total_expense FROM ExpenseTotal
                 ) AS CombinedData
                 GROUP BY Month
                 ORDER BY Month;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                ProfitReportModel obj = ProfitReportModel.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
