/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.Expenses;
import com.mycompany.softdevproject.model.ExpensesDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class ExpensesDao implements Dao<Expenses> {

    @Override
    public Expenses get(int id) {
        Expenses expenses = null;
        String sql = "SELECT * FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                expenses = expenses.fromRS(rs);
                ExpensesDetailDao rdd = new ExpensesDetailDao();
                ArrayList<ExpensesDetail> expensesDetail = (ArrayList<ExpensesDetail>) rdd.getAll("expenses_id=" + expenses.getId(), " expenses_detail_id ");
                expenses.setExpensesDetails(expensesDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expenses;
    }

    public List<ExpensesDetail> getByExpensesId(int id) {
        ArrayList<ExpensesDetail> list = new ArrayList();
        String sql = "SELECT * FROM expenses_detail WHERE expenses_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ExpensesDetail expensesDetail = ExpensesDetail.fromRS(rs);
                list.add(expensesDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Expenses> getAll() {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expenses expenses = Expenses.fromRS(rs);
                list.add(expenses);
                ExpensesDetailDao rdd = new ExpensesDetailDao();
                ArrayList<ExpensesDetail> expensesDetail = (ArrayList<ExpensesDetail>) rdd.getAll("expenses_id=" + expenses.getId(), " expenses_detail_id ");
                expenses.setExpensesDetails(expensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Expenses> getAll(String order) {
        ArrayList<Expenses> list = new ArrayList();
        String sql = "SELECT * FROM expenses  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expenses checkMat = Expenses.fromRS(rs);
                list.add(checkMat);
                ExpensesDetailDao rdd = new ExpensesDetailDao();
                ArrayList<ExpensesDetail> expensesDetail = (ArrayList<ExpensesDetail>) rdd.getAll("expenses_id=" + checkMat.getId(), " expenses_detail_id ");
                checkMat.setExpensesDetails(expensesDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Expenses save(Expenses obj) {
        String sql = "INSERT INTO expenses (expenses_total,employee_id)"
                + "VALUES(?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getEmployeeId());
            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Expenses update(Expenses obj) {
        String sql = "UPDATE expenses"
                + " SET expenses_name = ?,employee_id = ?"
                + " WHERE expenses_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.setInt(3, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Expenses obj) {
        String sql = "DELETE FROM expenses WHERE expenses_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Expenses> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
