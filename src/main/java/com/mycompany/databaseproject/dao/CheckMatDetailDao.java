/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.CheckMatDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anawi
 */
public class CheckMatDetailDao implements Dao<CheckMatDetail> {

    @Override
    public CheckMatDetail get(int id) {
        CheckMatDetail recieptdetail = null;
        String sql = "SELECT * FROM check_material_detail WHERE reciept_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptdetail = CheckMatDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptdetail;
    }

    public List<CheckMatDetail> getAll() {
        ArrayList<CheckMatDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMatDetail recieptdetail = CheckMatDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckMatDetail> getAll(String where, String order) {
        ArrayList<CheckMatDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMatDetail recieptdetail = CheckMatDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMatDetail> getAll(String order) {
        ArrayList<CheckMatDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMatDetail recieptdetail = CheckMatDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckMatDetail> getByCheckMatId(int id) {
        ArrayList<CheckMatDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_material_detail WHERE check_mat_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CheckMatDetail mat = CheckMatDetail.fromRS(rs);
                list.add(mat);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMatDetail save(CheckMatDetail obj) {

        String sql = "INSERT INTO check_material_detail (cmd_name,cmd_last_balance,cmd_remain,mat_id,check_mat_id)"
                + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getLastBalance());
            stmt.setInt(3, obj.getCmdRemain());
            stmt.setInt(4, obj.getMatId());
            stmt.setInt(5, obj.getCheckMatId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMatDetail update(CheckMatDetail obj) {
        String sql = "UPDATE check_material_detail"
                + " SET cmd_name = ?, cmd_remain = ?, cmd_last_balance = ?, mat_id = ?, check_mat_id = ?"
                + " WHERE cmd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getLastBalance());
            stmt.setInt(3, obj.getCmdRemain());
            stmt.setInt(4, obj.getMatId());
            stmt.setInt(5, obj.getCheckMatId());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMatDetail obj) {
        String sql = "DELETE FROM check_material_detail WHERE cmd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
