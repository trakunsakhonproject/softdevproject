/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.CheckMat;
import com.mycompany.softdevproject.model.CheckMatDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anawi
 */
public class CheckMatDao implements Dao<CheckMat>{
    @Override
    public CheckMat get(int id) {
        CheckMat checkMat = null;
        String sql = "SELECT * FROM check_material WHERE check_mat_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkMat = CheckMat.fromRS(rs);
                CheckMatDetailDao rdd = new CheckMatDetailDao();
                ArrayList<CheckMatDetail> checkMatDetails = (ArrayList<CheckMatDetail>) rdd.getAll("check_mat_id=" + checkMat.getId(), " cmd_id ");
                checkMat.setCheckMatDetails(checkMatDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkMat;
    }

    public List<CheckMat> getAll() {
        ArrayList<CheckMat> list = new ArrayList();
        String sql = "SELECT * FROM check_material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMat checkMat = CheckMat.fromRS(rs);
                list.add(checkMat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckMat> getAll(String where, String order) {
        ArrayList<CheckMat> list = new ArrayList();
        String sql = "SELECT * FROM check_material where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMat checkMat = CheckMat.fromRS(rs);
                list.add(checkMat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<CheckMat> getAll(String order) {
        ArrayList<CheckMat> list = new ArrayList();
        String sql = "SELECT * FROM check_material  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMat checkMat = CheckMat.fromRS(rs);
                list.add(checkMat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMat save(CheckMat obj) {

        String sql = "INSERT INTO check_material (employee_id)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMat update(CheckMat obj) {
        String sql = "UPDATE check_material"
                + " SET  employee_id = ?"
                + " WHERE check_mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckMat obj) {
        String sql = "DELETE FROM check_material WHERE check_mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
