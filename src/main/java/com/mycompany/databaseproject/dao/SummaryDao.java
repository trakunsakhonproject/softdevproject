/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.Summary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class SummaryDao implements Dao<Summary> {

    @Override
    public Summary get(int id) {
        Summary summary = null;
        String sql = "SELECT * FROM summary_salary WHERE ss_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                summary = Summary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return summary;
    }

    @Override
    public List<Summary> getAll() {
        ArrayList<Summary> list = new ArrayList();
        String sql = "SELECT * FROM summary_salary ORDER BY ss_id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Summary summary = Summary.fromRS(rs);
                list.add(summary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Summary> PayEmployee() {
        ArrayList<Summary> list = new ArrayList<>();
        String sql = "SELECT SUBSTR(cio_date, 1, 4) AS year, SUBSTR(cio_date, 6, 2) AS month, employee_id, SUM(cio_time_hour) AS total_hours FROM check_in_out GROUP BY SUBSTR(cio_date, 1, 4), SUBSTR(cio_date, 6, 2), employee_id;";
        try (Connection conn = DatabaseHelper.getConnect(); Statement stmt = conn.createStatement(); ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                Summary summary = Summary.fromRS(rs);
                list.add(summary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     public int getTotalMonth(int month) {
        int total = 0;
        String sql = "SELECT SUM(ss_total) AS Total FROM summary_salary WHERE strftime('%m', ss_date) = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, String.format("%02d", month)); // Ensuring the month is in two-digit format
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                total = rs.getInt("Total");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return total;
    }

    @Override
    public Summary save(Summary obj) {
        String sql = "INSERT INTO summary_salary (employee_id, employee_name, employee_hour_wage, ss_total, ss_work_hour )"
                + "VALUES(?, ?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getEmployeeName());
            stmt.setDouble(3, obj.getEmployeeHourWage());
            stmt.setDouble(4, obj.getTotal());
            stmt.setDouble(5, obj.getWorkHour());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Summary update(Summary obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Summary obj) {
        String sql = "DELETE FROM summary_salary WHERE ss_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Summary> getAll(String where, String order) {
        ArrayList<Summary> list = new ArrayList();
        String sql = "SELECT * FROM product  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Summary summary = Summary.fromRS(rs);

                list.add(summary);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
