/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.helper.DatabaseHelper;
import com.mycompany.softdevproject.model.CheckInOut;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut get(int id) {
        CheckInOut checkInOut = null;
        String sql = "SELECT * FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkInOut = CheckInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }

    public List<CheckInOut> getAllByEmpId(int id) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out WHERE cio_time_out NOT null AND ss_id IS null AND employee_id=" + id + " ORDER BY cio_date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAllByEmp() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out GROUP BY employee_id";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {
        String sql = "INSERT INTO check_in_out (cio_time_out, cio_time_hour, employee_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDate(1, (Date) obj.getTimeOut());
            stmt.setInt(2, obj.getTimeHour());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE check_in_out"
                + " SET ss_id = ?"
                + " WHERE cio_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSsId());
            stmt.setInt(2, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public CheckInOut checkOut(CheckInOut obj) {
        String updateCheckOutTimeSql = "UPDATE check_in_out"
                + " SET cio_time_out=?"
                + " WHERE cio_id = ?";
        String updateWorkHoursSql = "UPDATE  check_in_out"
                + " SET cio_time_hour=?"
                + " WHERE cio_id= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(updateCheckOutTimeSql);
            // Format checkOutDatetime
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(obj.getTimeOut());
            int thaiYear = calendar.get(Calendar.YEAR) - 543;
            calendar.set(Calendar.YEAR, thaiYear);
            Timestamp checkOutTimestamp = new Timestamp(calendar.getTime().getTime());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String checkOutDatetimeString = dateFormat.format(checkOutTimestamp);
            stmt.setString(1, checkOutDatetimeString);
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
            CheckInOut updatedCheckInOut = get(obj.getId());
            long timeDifference = updatedCheckInOut.getTimeOut().getTime() - updatedCheckInOut.getTimeIn().getTime();
            int totalWorkHours = (int) (timeDifference / 3600000);
            PreparedStatement stmt2 = conn.prepareStatement(updateWorkHoursSql);
            stmt2.setInt(1, totalWorkHours);
            stmt2.setInt(2, updatedCheckInOut.getId());
            stmt2.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM check_in_out WHERE cio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckInOut> getAll(String where, String order) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM check_in_out  WHERE  " + where + " ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);

                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
