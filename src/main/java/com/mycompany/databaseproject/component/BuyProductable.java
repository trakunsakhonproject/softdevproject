/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.databaseproject.component;

import com.mycompany.softdevproject.model.Product;

/**
 *
 * @author kunpo
 */
public interface BuyProductable {
    public void buy(Product product,String productName,String size,String type,String sweet,float price ,int qty);
}
