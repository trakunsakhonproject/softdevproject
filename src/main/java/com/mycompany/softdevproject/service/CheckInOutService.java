/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.CheckInOutDao;
import com.mycompany.softdevproject.model.CheckInOut;
import java.util.Date;
import java.util.List;

/**
 *
 * @author User
 */
public class CheckInOutService {
      public List<CheckInOut> getCheckInOut() {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.getAll();
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        return checkInOutDao.delete(editedCheckInOut);
    }
    public CheckInOut getById(int id) {
        CheckInOutDao checkMatDao = new CheckInOutDao();
        return checkMatDao.get(id);
    }
    public List<CheckInOut> getCheckInOutsByEmployeeId(int id) {
        CheckInOutDao checkMatDao = new CheckInOutDao();
        return checkMatDao.getAllByEmpId(id);
    }
    
    public CheckInOut getCheckOut(CheckInOut editedCheckInOut) {
        Date date = new Date();
        CheckInOutDao checkInOutDao = new CheckInOutDao();
        editedCheckInOut.setTimeOut(date);
        return checkInOutDao.checkOut(editedCheckInOut);
    }
}
