/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.company.databaseproject.testReport.ProfitReportModel;
import com.mycompany.databaseproject.dao.ReceiptDao;
import com.mycompany.databaseproject.dao.ReceiptDetailDao;
import com.mycompany.softdevproject.model.Member;
import com.mycompany.softdevproject.model.Receipt;
import com.mycompany.softdevproject.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptService {

    public Receipt getById(int id) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.get(id);
    }

    public List<Receipt> getReceipts() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll(" receipt_id DESC");
    }

    public Receipt addNew(Receipt editedReceipt, int userId) {
        editedReceipt.setUserId(userId);
        MemberService memberService = new MemberService();
        ReceiptDao receiptDao = new ReceiptDao();
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        Receipt receipt = receiptDao.save(editedReceipt);
        for (ReceiptDetail rd : editedReceipt.getReceiptDetails()) {
            rd.setReceiptId(receipt.getId());
            receiptDetailDao.save(rd);
        }
        if (receipt.getCustomerId() > 0) {
            Member member = memberService.getId(receipt.getCustomerId());
            member.setPoint(member.getPoint() + receipt.getTotalQty());
            memberService.update(member);
        }
        return receipt;

    }

    public Receipt update(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedReceipt);
    }

    public int delete(Receipt editedReceipt) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedReceipt);
    }

    public List<ProfitReportModel> Profit() {
        ReceiptDao recieptDao = new ReceiptDao();
        return recieptDao.Profit();
    }
}
