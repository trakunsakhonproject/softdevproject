/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.softdevproject.model.Employee;
import java.util.List;

/**
 *
 * @author User
 */
public class EmployeeService {

    public static Employee currentUser;

    public Employee login(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if (employee != null && employee.getPassword().equals(password)) {
            currentUser = employee;
            System.out.println("Succes");
            System.out.println(currentUser.getName());
            return employee;
        } else {
            System.out.println("No");
        }
        return null;
    }

    public Employee CheckInOut(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if (employee != null && employee.getPassword().equals(password)) {
            currentUser = employee;
            System.out.println("CheckInOut");
            System.out.println(currentUser.getName());
            return employee;
        } else {
            System.out.println("No");
        }
        return null;
    }

    public Employee getId(int id) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(id);
    }

    public static Employee getCurrentUser() {
        return new Employee("" + currentUser.getName(), "admin", "password", "Male", 1);
    }

    public List<Employee> getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll();
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
}
