/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.BillDao;
import com.mycompany.databaseproject.dao.BillDetailDao;
import com.mycompany.databaseproject.dao.CheckMatDao;
import com.mycompany.softdevproject.model.Bill;
import com.mycompany.softdevproject.model.BillDetail;
import com.mycompany.softdevproject.model.CheckMat;
import java.util.List;

/**
 *
 * @author user
 */
public class BillService {

    public List<Bill> getBill() {
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id DESC");
    }

    public List<Bill> getBillDESC(String order) {
        BillDao billDao = new BillDao();
        return billDao.getAllDESC(" bill_id DESC");
    }

    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        return billDao.get(id);
    }

    public Bill addNew(Bill editedBill, int userId) {
        editedBill.setUserId(userId);
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();

        Bill bill = billDao.save(editedBill);
        for (BillDetail bd : editedBill.getBillDetails()) {
            bd.setBillId(bill.getId());
            billDetailDao.save(bd);
        }
        return bill;
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
}
