/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.SummaryDao;
import com.mycompany.softdevproject.model.CheckInOut;
import com.mycompany.softdevproject.model.Summary;
import java.util.List;

/**
 *
 * @author User
 */
public class SummaryService {

    SummaryDao summaryDao = new SummaryDao();

    public List<Summary> getSummary() {
        SummaryDao summaryrDao = new SummaryDao();
        return summaryrDao.getAll();
    }

    public Summary addNew(Summary salary) {
            Summary savedSalary = summaryDao.save(salary);
            CheckInOutService checkInOutService = new CheckInOutService();
            for (CheckInOut c : salary.getCheckinouts()) {
                c.setSsId(savedSalary.getId());
                checkInOutService.update(c);
                System.out.println(c);
            }
            return savedSalary;
    }

    public Summary update(Summary editedSummary) {
        SummaryDao summaryDao = new SummaryDao();
        return summaryDao.update(editedSummary);
    }

    public int delete(Summary editedSummary) {
        SummaryDao summaryDao = new SummaryDao();
        return summaryDao.delete(editedSummary);
    }

    public List<Summary> PayEmployee() {
        SummaryDao summaryDao = new SummaryDao();
        return summaryDao.PayEmployee();
    }
}
