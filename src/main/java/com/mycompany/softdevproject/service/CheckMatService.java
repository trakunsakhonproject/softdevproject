/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.CheckMatDao;
import com.mycompany.databaseproject.dao.CheckMatDetailDao;
import com.mycompany.softdevproject.model.CheckMat;
import com.mycompany.softdevproject.model.CheckMatDetail;
import com.mycompany.softdevproject.model.Meterial;
import java.util.List;

/**
 *
 * @author anawi
 */
public class CheckMatService {

    public CheckMat getById(int id) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.get(id);
    }

    public List<CheckMat> getCheckMats() {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.getAll(" check_mat_id DESC");
    }

    public CheckMat addNew(CheckMat editedCheckMat, int userId) {
        MeterialService materialService = new MeterialService();
        CheckMatDao checkMatDao = new CheckMatDao();
        CheckMatDetailDao checkMatDetailDao = new CheckMatDetailDao();
        editedCheckMat.setEmployeeId(userId);
        CheckMat checkMat = checkMatDao.save(editedCheckMat);
        for (CheckMatDetail rd : editedCheckMat.getCheckMatDetails()) {
            Meterial mat = materialService.getId(rd.getMatId());
            mat.setBalance(rd.getCmdRemain());
            materialService.update(mat);
            rd.setCheckMatId(checkMat.getId());
            checkMatDetailDao.save(rd);
        }
        return checkMat;
    }

    public CheckMat update(CheckMat editedCheckMat) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.update(editedCheckMat);
    }

    public int delete(CheckMat editedCheckMat) {
        CheckMatDao checkMatDao = new CheckMatDao();
        return checkMatDao.delete(editedCheckMat);
    }
}
