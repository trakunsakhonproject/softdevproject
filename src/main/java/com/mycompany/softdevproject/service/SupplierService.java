/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.SupplierDao;
import com.mycompany.softdevproject.model.Supplier;
import java.util.List;

/**
 *
 * @author user
 */
public class SupplierService {

    public List<Supplier> getSupplier() {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.getAll();
    }

    public Supplier addNew(Supplier editedSupplier) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.save(editedSupplier);
    }

    public Supplier update(Supplier editedSupplier) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.update(editedSupplier);
    }

    public int delete(Supplier editedSupplier) {
        SupplierDao supplierDao = new SupplierDao();
        return supplierDao.delete(editedSupplier);
    }
     public Supplier getByName(String name){
        SupplierDao memDao = new SupplierDao();
       return  memDao.getByName(name);
    }

}
