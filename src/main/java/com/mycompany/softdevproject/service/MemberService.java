/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.MemberDao;

import com.mycompany.softdevproject.model.Member;

import java.util.List;

/**
 *
 * @author user
 */
public class MemberService {

    public List<Member> getMember() {
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll();
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);
    }

    public Member getByPhone(String tel) {
        MemberDao memDao = new MemberDao();
        return memDao.getByPhone(tel);
    }

    public Member getId(int id) {
        MemberDao memDao = new MemberDao();
        return memDao.get(id);
    }
}
