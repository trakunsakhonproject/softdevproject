/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.ExpensesDao;
import com.mycompany.databaseproject.dao.ExpensesDetailDao;
import com.mycompany.softdevproject.model.Expenses;
import com.mycompany.softdevproject.model.ExpensesDetail;
import java.util.List;

/**
 *
 * @author user
 */
public class ExpensesService {

    public List<Expenses> getExpenses() {
        ExpensesDao billDao = new ExpensesDao();
        return billDao.getAll();
    }
     public List<Expenses> getExpensesDESC() {
        ExpensesDao billDao = new ExpensesDao();
        return billDao.getAll(" expenses_id DESC");
    }

    public Expenses getById(int id) {
        ExpensesDao billDao = new ExpensesDao();
        return billDao.get(id);
    }

    public Expenses addNew(Expenses editedExpenses, int userId) {
        editedExpenses.setEmployeeId(userId);
        ExpensesDao billDao = new ExpensesDao();
        ExpensesDetailDao billDetailDao = new ExpensesDetailDao();

        Expenses bill = billDao.save(editedExpenses);
        for (ExpensesDetail bd : editedExpenses.getExpensesDetails()) {
            bd.setExpensesId(bill.getId());
            billDetailDao.save(bd);
        }
        return bill;
    }

    public Expenses update(Expenses editedExpenses) {
        ExpensesDao billDao = new ExpensesDao();
        return billDao.update(editedExpenses);
    }

    public int delete(Expenses editedExpenses) {
        ExpensesDao billDao = new ExpensesDao();
        return billDao.delete(editedExpenses);
    }
}
