/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.ProductDao;
import com.mycompany.softdevproject.model.Product;
import com.mycompany.softdevproject.model.ProductReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author anawi
 */
public class ProductService {

    public List<Product> getProduct() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }
    private ProductDao productDao = new ProductDao();

    public ArrayList<Product> getProductOderByName(int cat) {
        return (ArrayList<Product>) productDao.getAll(cat);
    }

    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        if (editedProduct != null) {
            ProductDao proDao = new ProductDao();
            return proDao.update(editedProduct);
        } else {
            System.err.println("Cannot update a null product.");
            return null;
        }
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
     public List<ProductReport> getProductBestSeller() {
        ProductDao productdao = new ProductDao();
        return productdao.getProductBestSeller(10);
    }
     public List<ProductReport> getProductBestSeller(String begin, String end) {
        ProductDao productdao = new ProductDao();
        return productdao.getProductBestSeller(begin,end,10);
    }
}
