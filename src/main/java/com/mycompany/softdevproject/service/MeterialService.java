/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.MeterialDao;

import com.mycompany.softdevproject.model.Meterial;
import java.util.ArrayList;

import java.util.List;

/**
 *
 * @author Lenovo
 */
public class MeterialService {
    public List<Meterial> getMeterial() {
        MeterialDao meterialDao = new MeterialDao();
        return meterialDao.getAll();
    }
    private MeterialDao materialDao = new MeterialDao();
    
    public ArrayList<Meterial> getMaterialOderByName() {
        return (ArrayList<Meterial>) materialDao.getAll(" mat_name ASC ");
    }

    public Meterial addNew(Meterial editedMeterial) {
        MeterialDao meterialDao = new MeterialDao();
        return meterialDao.save(editedMeterial);
    }

    public Meterial update(Meterial editedMeterial) {
        MeterialDao meterialDao = new MeterialDao();
        return meterialDao.update(editedMeterial);
    }

    public int delete(Meterial editedMeterial) {
        MeterialDao meterialDao = new MeterialDao();
        return meterialDao.delete(editedMeterial);
    }
    public Meterial getId(int id){
        MeterialDao matDao = new MeterialDao();
        return matDao.get(id);
    
    }
}

