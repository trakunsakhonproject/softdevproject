/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.service;

import com.mycompany.databaseproject.dao.CategoryDao;
import com.mycompany.softdevproject.model.Category;
import java.util.List;

/**
 *
 * @author Coke
 */
public class CategoryService {
    public List<Category> getCategory() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll();
    }
    public Category getName(String name){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(name);
    }
    public Category getId(int id){
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.get(id);
    }
}
