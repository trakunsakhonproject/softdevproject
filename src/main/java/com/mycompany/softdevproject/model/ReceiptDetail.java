/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.MeterialDao;
import com.mycompany.databaseproject.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class ReceiptDetail {

    private int id;
    private int productId;
    private String productName;
    private float productPrice;
    private String productSize;
    private String producType;
    private String productSweet;
    private int qty;
    private float totalPrice;
    private int receiptId;
    private Receipt reciept;
    private Product product;

    public ReceiptDetail(int id, int productId, String productName, float productPrice, String productSize, String productType, String productSweet, int qty, float totalPrice, int receiptId) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.producType = productType;
        this.productSweet = productSweet;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.receiptId = receiptId;
    }

    public ReceiptDetail(int productId, String productName, float productPrice, String productSize, String productType, String productSweet, int qty, float totalPrice, int receiptId) {
        this.id = -1;
        this.productId = productId;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.producType = productType;
        this.productSweet = productSweet;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.receiptId = receiptId;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.productId = 0;
        this.productName = "";
        this.productPrice = 0;
        this.productSize = "";
        this.producType = "";
        this.productSweet = "";
        this.qty = 0;
        this.totalPrice = 0;
        this.receiptId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * productPrice;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProducType() {
        return producType;
    }

    public void setProducType(String producType) {
        this.producType = producType;
    }

    public String getProductSweet() {
        return productSweet;
    }

    public void setProductSweet(String productSweet) {
        this.productSweet = productSweet;
    }

    public Receipt getReciept() {
        return reciept;
    }

    public void setReciept(Receipt reciept) {
        this.reciept = reciept;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", productId=" + productId + ", productName=" + productName + ", productPrice=" + productPrice + ", productSize=" + productSize + ", producType=" + producType + ", productSweet=" + productSweet + ", qty=" + qty + ", totalPrice=" + totalPrice + ", receiptId=" + receiptId + ", reciept=" + reciept + ", product=" + product + '}';
    }
    
    

    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail reciepdetail = new ReceiptDetail();
        try {
            reciepdetail.setId(rs.getInt("receipt_detail_id"));
            reciepdetail.setProductId(rs.getInt("product_id"));
            reciepdetail.setProductName(rs.getString("product_name"));
            reciepdetail.setProductPrice(rs.getFloat("product_price"));
            reciepdetail.setProductSize(rs.getString("product_size"));
            reciepdetail.setProducType(rs.getString("product_type"));
            reciepdetail.setProductSweet(rs.getString("product_sweet"));
            reciepdetail.setQty(rs.getInt("qty"));
            reciepdetail.setTotalPrice(rs.getFloat("total_price"));
            reciepdetail.setReceiptId(rs.getInt("receipt_id"));
            ProductDao productDao = new ProductDao();
            Product product = productDao.get(rs.getInt("product_id"));
            //Popuulation
            reciepdetail.setProduct(product);

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciepdetail;
    }
}
