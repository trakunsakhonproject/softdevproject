/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.MeterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class ExpensesDetail {
    private int id;
    private String name;
    private float total;
    private int expensesId;

    public ExpensesDetail(int id, String name, float total, int expensesId) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.expensesId = expensesId;
    }
    public ExpensesDetail( String name, float total, int expensesId) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.expensesId = expensesId;
    }
     public ExpensesDetail(String name, float total) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.expensesId = 0;
    }
     public ExpensesDetail() {
        this.id = -1;
        this.name = "";
        this.total = 0;
        this.expensesId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getExpensesId() {
        return expensesId;
    }

    public void setExpensesId(int expensesId) {
        this.expensesId = expensesId;
    }

    @Override
    public String toString() {
        return "ExpensesDetail{" + "id=" + id + ", name=" + name + ", total=" + total + ", expensesId=" + expensesId + '}';
    }
    
    public static ExpensesDetail fromRS(ResultSet rs) {
        ExpensesDetail expensesdetail = new ExpensesDetail();
        try {
            expensesdetail.setId(rs.getInt("expenses_detail_id"));
            expensesdetail.setName(rs.getString("expenses_detail_name"));
            expensesdetail.setTotal(rs.getFloat("expenses_detail_total"));
            expensesdetail.setExpensesId(rs.getInt("expenses_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expensesdetail;
    }
}
