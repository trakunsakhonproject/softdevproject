/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.CheckMatDetailDao;
import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.MeterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class CheckMat {

    private int id;
    private Timestamp createdDate;
    private int employeeId;
    private Employee user;
    private ArrayList<CheckMatDetail> checkMatDetails = new ArrayList();

    public CheckMat(int id, Timestamp createdDate, int employeeId) {
        this.id = id;
        this.createdDate = createdDate;
        this.employeeId = employeeId;
    }

    public CheckMat(int employeeId) {
        this.id = -1;
        this.createdDate = null;
        this.employeeId = employeeId;
    }

    public CheckMat(Timestamp createdDate, int employeeId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.employeeId = employeeId;
    }
    public CheckMat() {
        this.id = -1;
        this.createdDate = null;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public Employee getUser() {
        return user;
    }

    public void setUser(Employee user) {
        this.user = user;
    }
    

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public ArrayList<CheckMatDetail> getCheckMatDetails() {
        return checkMatDetails;
    }

    public void setCheckMatDetails(ArrayList<CheckMatDetail> checkMatDetails) {
        this.checkMatDetails = checkMatDetails;
    }

    @Override
    public String toString() {
        return "CheckMat{" + "id=" + id + ", createdDate=" + createdDate + ", employeeId=" + employeeId + ", user=" + user + ", checkMatDetails=" + checkMatDetails + '}';
    }
    public void adddCheckMatDetail(CheckMatDetail checkMatDetail){
        checkMatDetails.add(checkMatDetail);
    }
    
    
    public static CheckMat fromRS(ResultSet rs) {
       CheckMat checkMat = new CheckMat();
        try {
            checkMat.setId(rs.getInt("check_mat_id"));
            checkMat.setCreatedDate(rs.getTimestamp("check_mat_date"));
            checkMat.setEmployeeId(rs.getInt("employee_id"));
            //Popuulation
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(checkMat.getEmployeeId());
            checkMat.setUser(employee);
            CheckMatDetailDao checkMatDao = new CheckMatDetailDao();
            checkMat.setCheckMatDetails((ArrayList<CheckMatDetail>) checkMatDao.getByCheckMatId(checkMat.getId()));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMat.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMat;
    }
    
}
