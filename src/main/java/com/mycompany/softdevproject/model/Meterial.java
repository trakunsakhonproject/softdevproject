/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Meterial {
     private int id;
    private String name;
    private int balance;
    private int minimum;

    public Meterial(String name, int balance, int minimum) {
        this.id = -1;
        this.name = name;
        this.balance = balance;
        this.minimum = minimum;
    }
    

    public Meterial() {
        this.id = -1;
        this.name = "";
        this.balance = 0;
        this.minimum = 0;
    }

    @Override
    public String toString() {
        return "Meterial{" + "id=" + id + ", name=" + name + ", balance=" + balance + ", minimum=" + minimum + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public Meterial(int id, String name, int balance, int minimum) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.minimum = minimum;
    }

    public static Meterial fromRS(ResultSet rs) {
        Meterial meterial = new Meterial();
        try {
            meterial.setId(rs.getInt("mat_id"));
            meterial.setName(rs.getString("mat_name"));
            meterial.setBalance(rs.getInt("mat_balance"));
            meterial.setMinimum(rs.getInt("mat_minimum"));
        } catch (SQLException ex) {
            Logger.getLogger(Meterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        
        return meterial;
    }
}

