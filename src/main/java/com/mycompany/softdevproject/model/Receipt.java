/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.MemberDao;
import com.mycompany.databaseproject.dao.ReceiptDao;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class Receipt {

    private int id;
    private Timestamp createdDate;
    private float total;
    private float subtotal;
    private float cash;
    private int totalQty;
    private float discount;
    private String payment;
    private float change;
    private int userId;
    private int customerId;
    private Employee user;
    private Member customer;
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList();

    public Receipt(int id, Timestamp createdDate, float total, float subtotal, float cash, int totalQty, float discount, String payment, float change, int userId, int customerId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.totalQty = totalQty;
        this.discount = discount;
        this.payment = payment;
        this.change = change;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Receipt(float total, float subtotal, float cash, int totalQty, float discount, String payment, float change, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.totalQty = totalQty;
        this.discount = discount;
        this.payment = payment;
        this.change = change;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Receipt(float cash, int userId, int customerId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.subtotal = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.discount = 0;
        this.payment = "cash";
        this.change = 0;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Receipt(Timestamp createdDate, float total, float subtotal, float cash, int totalQty, float discount, String payment, float change, int userId, int customerId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.subtotal = subtotal;
        this.cash = cash;
        this.totalQty = totalQty;
        this.discount = discount;
        this.payment = payment;
        this.change = change;
        this.userId = userId;
        this.customerId = customerId;

    }

    public Receipt() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.subtotal = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.discount = 0;
        this.payment = "cash";
        this.change = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Employee getUser() {
        return user;
    }

    public void setUser(Employee user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Member getCustomer() {
        return customer;
    }

    public void setCustomer(Member customer) {
        this.customer = customer;
        this.customerId = customer.getId();
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList<ReceiptDetail> receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public float getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(float subtotal) {
        this.subtotal = subtotal;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", subtotal=" + subtotal + ", cash=" + cash + ", totalQty=" + totalQty + ", discount=" + discount + ", payment=" + payment + ", change=" + change + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + ", receiptDetails=" + receiptDetails + '}';
    }

    public void addReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.add(receiptDetail);
        calculateTotal();
    }

    public void addReceiptDetail(Product product, String productName ,String size,String type,String sweetLevel ,float price, int qty ) {
        ReceiptDetail rd = new ReceiptDetail(product.getId(), productName, price, 
                size, type, sweetLevel, qty, qty * price, -1);

        receiptDetails.add(rd);
        calculateTotal();
    }

    public void delReceiptDetail( ) {
        receiptDetails.clear();
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        float subtotal = 0.0f;
        for (ReceiptDetail rd : receiptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
            subtotal += rd.getTotalPrice();
        }
        this.totalQty = totalQty;
        this.total = total -= discount;
        this.subtotal = subtotal;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setCreatedDate(rs.getTimestamp("receipt_date"));
            receipt.setTotal(rs.getFloat("receipt_total"));
            receipt.setTotal(rs.getFloat("receipt_subtotal"));
            receipt.setCash(rs.getFloat("receipt_cash"));
            receipt.setTotalQty(rs.getInt("receipt_total_qty"));
            receipt.setDiscount(rs.getFloat("receipt_discount"));
            receipt.setPayment(rs.getString("receipt_payment"));
            receipt.setUserId(rs.getInt("employee_id"));
            int memberId = rs.getInt("member_id");
            if (!rs.wasNull()) {
                receipt.setCustomerId(memberId);
            }
            MemberDao memberDao = new MemberDao();
            EmployeeDao employeeDao = new EmployeeDao();
            Member member = memberDao.get(receipt.getCustomerId());
            Employee employee = employeeDao.get(receipt.getUserId());
            receipt.setUser(employee);
            ReceiptDao recDao = new ReceiptDao();
            receipt.setReceiptDetails((ArrayList<ReceiptDetail>) recDao.getByReceiptId(receipt.getId()));

            // Handle null member case
            if (member != null) {
                receipt.setCustomer(member);
            } else {
                // Set a default member or handle it in an appropriate way for your application
                receipt.setCustomer(new Member()); // Example: Setting an empty member object
            }

            // Handle null employee case similarly if necessary
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }
}
