/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.CheckMatDao;
import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.MemberDao;
import com.mycompany.databaseproject.dao.MeterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class CheckMatDetail {

    private int id;
    private String name;
    private int cmdRemain;
    private int lastBalance;
    private int matId;
    private int checkMatId;
    private Meterial material;
    private CheckMat checkmat;

    public CheckMatDetail(int id, String name, int cmdRemain, int lastBalance, int matId, int checkMatId) {
        this.id = id;
        this.name = name;
        this.cmdRemain = cmdRemain;
        this.lastBalance = lastBalance;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }

    public CheckMatDetail(String name, int cmdRemain, int lastBalance, int matId, int checkMatId) {
        this.id = -1;
        this.name = name;
        this.cmdRemain = cmdRemain;
        this.lastBalance = lastBalance;
        this.matId = matId;
        this.checkMatId = checkMatId;
    }

    public CheckMatDetail() {
        this.id = -1;
        this.name = "";
        this.cmdRemain = 0;
        this.lastBalance = 0;
        this.matId = 0;
        this.checkMatId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCmdRemain() {
        return cmdRemain;
    }

    public void setCmdRemain(int cmdRemain) {
        this.cmdRemain = cmdRemain;
    }

    public int getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(int lastBalance) {
        this.lastBalance = lastBalance;
    }

    public int getMatId() {
        return matId;
    }

    public void setMatId(int matId) {
        this.matId = matId;
    }

    public int getCheckMatId() {
        return checkMatId;
    }

    public void setCheckMatId(int checkMatId) {
        this.checkMatId = checkMatId;
    }

    public Meterial getMaterial() {
        return material;
    }

    public void setMaterial(Meterial material) {
        this.matId = material.getId();
        this.material = material;
    }

    public CheckMat getCheckmat() {
        return checkmat;
    }

    public void setCheckmat(CheckMat checkmat) {
        this.checkmat = checkmat;
    }

    @Override
    public String toString() {
        return "CheckMatDetail{" + "id=" + id + ", name=" + name + ", cmdRemain=" + cmdRemain + ", lastBalance=" + lastBalance + ", matId=" + matId + ", checkMatId=" + checkMatId + ", material=" + material + ", checkmat=" + checkmat + '}';
    }

    public static CheckMatDetail fromRS(ResultSet rs) {
        CheckMatDetail checkMatDetail = new CheckMatDetail();
        try {
            checkMatDetail.setId(rs.getInt("cmd_id"));
            checkMatDetail.setName(rs.getString("cmd_name"));
            checkMatDetail.setCmdRemain(rs.getInt("cmd_remain"));
            checkMatDetail.setLastBalance(rs.getInt("cmd_last_balance"));
            MeterialDao matDao = new MeterialDao();
            Meterial material = matDao.get(rs.getInt("mat_id"));
            //Popuulation
            checkMatDetail.setMaterial(material);
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMatDetail;
    }

}
