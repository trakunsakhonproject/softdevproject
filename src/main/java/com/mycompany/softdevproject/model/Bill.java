/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.BillDao;
import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.MemberDao;
import com.mycompany.databaseproject.dao.SupplierDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Bill {

    private int id;
    private Timestamp createdDate;
    private float total;
    private float subTotal;
    private float discount;
    private int totalQty;
    private int userId;
    private int supplierId;
    private Employee user;
    private Supplier supplier;
    private ArrayList<BillDetail> billDetails = new ArrayList();

    public Bill(int id, Timestamp createdDate, float total, float subTotal, float discount, int totalQty, int userId, int supplierId, Employee user, Supplier supplier) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = userId;
        this.supplierId = supplierId;
    }

    public Bill(Timestamp createdDate, float total, float subTotal, float discount, int totalQty, int userId, int supplierId, Employee user, Supplier supplier) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.subTotal = subTotal;
        this.discount = discount;
        this.totalQty = totalQty;
        this.userId = userId;
        this.supplierId = supplierId;
    }

    public Bill() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.subTotal = 0;
        this.discount = 0;
        this.totalQty = 0;
        this.userId = userId;
        this.supplierId = supplierId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }


    public int getUserId() {
        return userId;

    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public Employee getUser() {
        return user;
    }

    public void setUser(Employee user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
        this.supplierId = supplier.getId();
    }

    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList<BillDetail> billDetails) {
        this.billDetails = billDetails;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", subTotal=" + subTotal + ", discount=" + discount + ", totalQty=" + totalQty + ", userId=" + userId + ", supplierId=" + supplierId + ", user=" + user + ", supplier=" + supplier + ", billDetails=" + billDetails + '}';
    }

    
    

    public void addBillDetail(BillDetail billDetail) {
        billDetails.add(billDetail);
        calculateTotal();
    }

    public void addBillDetail(Meterial material, int qty, float price) {
        BillDetail rd = new BillDetail(material.getId(), material.getName(),
                price, qty, qty * price, -1);
        billDetails.add(rd);
        calculateTotal();
    }

    public void delBillDetail() {
        billDetails.clear();
        discount = 0;
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        float subtotal = 0.0f;
        for (BillDetail rd : billDetails) {
            total += rd.getMaterialPrice() * rd.getAmount();
            totalQty += rd.getAmount();
        }
        this.totalQty = totalQty;
        this.total = total -= discount;
        this.subTotal = total;
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setCreatedDate(rs.getTimestamp("bill_date"));
            bill.setTotal(rs.getFloat("bill_total"));
            bill.setSubTotal(rs.getFloat("bill_subTotal"));
            bill.setDiscount(rs.getFloat("bill_discount"));
            bill.setTotalQty(rs.getInt("bill_totalQty"));
            bill.setUserId(rs.getInt("employee_id"));
            bill.setSupplierId(rs.getInt("supplier_id"));
            BillDao billDao = new BillDao();
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(bill.getUserId());
            bill.setUser(employee);
            SupplierDao supplierDao = new SupplierDao();
            Supplier supplier = supplierDao.get(bill.getSupplierId());
            bill.setSupplier(supplier);
            bill.setBillDetails((ArrayList<BillDetail>) billDao.getByBillId(bill.getId()));

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
