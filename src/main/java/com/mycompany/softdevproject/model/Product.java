/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.CategoryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class Product {
    private int id;
    private String name;
    private float price;
    private String type;
    private int categoryId;
    private Category category;

    public Product(int id, String name, int price, String type, int categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Product(String name, int price, String type, int categoryId) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.type = "";
        this.categoryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

  public Category getCategory(){
        return category;
   }
  public void setCategory(Category category) {
      this.category = category;
       this.categoryId = category.getId();
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", type=" + type + ", categoryId=" + categoryId + ", category=" + category + '}';
    }

    
  

    public static Product fromRs(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getFloat("product_price"));
            product.setType(rs.getString("product_type"));
            product.setCategoryId(rs.getInt("category_id"));
            CategoryDao categoryDao = new CategoryDao();
            Category category = categoryDao.get(product.getCategoryId());
            product.setCategory(category);
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

}
