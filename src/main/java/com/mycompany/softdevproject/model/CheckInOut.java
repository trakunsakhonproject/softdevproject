/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.SummaryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class CheckInOut {

    private int id;
    private Date date;
    private Date timeIn;
    private Date timeOut;
    private int timeHour;
    private int employeeId;
    private int ssId;
    private Employee employee;
    private Summary summary;

    public CheckInOut(int id, Date date, Date timeIn, Date timeOut, int timeHour, int employeeId, int ssId) {
        this.id = id;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.timeHour = timeHour;
        this.employeeId = employeeId;
        this.ssId = ssId;
    }

    public CheckInOut(Date date, Date timeIn, Date timeOut, int timeHour, int employeeId, int ssId) {
        this.id = -1;
        this.date = date;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.timeHour = timeHour;
        this.employeeId = employeeId;
        this.ssId = ssId;
    }

    public CheckInOut(int employeeId) {
        this.id = -1;
        this.date = null;
        this.timeIn = null;
        this.timeOut = null;
        this.timeHour = 0;
        this.employeeId = employeeId;
        this.ssId = 0;
    }

    public CheckInOut() {
        this.id = -1;
        this.date = null;
        this.timeIn = null;
        this.timeOut = null;
        this.timeHour = 0;
        this.employeeId = -1;
        this.ssId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public int getTimeHour() {
        return timeHour;
    }

    public void setTimeHour(int timeHour) {
        this.timeHour = timeHour;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getSsId() {
        return ssId;
    }

    public void setSsId(int ssId) {
        this.ssId = ssId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", timeIn=" + timeIn + ", timeOut=" + timeOut + ", timeHour=" + timeHour + ", employeeId=" + employeeId + ", ssId=" + ssId + ", employee=" + employee + ", summary=" + summary + '}';
    }

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("cio_id"));
            checkInOut.setDate(rs.getDate("cio_date"));
            checkInOut.setTimeIn(rs.getDate("cio_time_in"));
            checkInOut.setTimeOut(rs.getDate("cio_time_out"));
            checkInOut.setTimeHour(rs.getInt("cio_time_hour"));
            checkInOut.setEmployeeId(rs.getInt("employee_id"));
            checkInOut.setSsId(rs.getInt("ss_id"));
            EmployeeDao empDao = new EmployeeDao();
            Employee emp = empDao.get(checkInOut.getEmployeeId());
            checkInOut.setEmployee(emp);
            SummaryDao summaryDao = new SummaryDao();
            Summary sum = summaryDao.get(checkInOut.getSsId());
            checkInOut.setSummary(sum);
            

        } catch (SQLException ex) {
            Logger.getLogger(Summary.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkInOut;

    }

}
