/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class LowStockReport {
    private int id;
    private String name;
    private int remian;
    private int minimum;
    private String status;

    public LowStockReport(int id, String name, int remian, int minimum, String status) {
        this.id = id;
        this.name = name;
        this.remian = remian;
        this.minimum = minimum;
        this.status = status;
    }
    public LowStockReport(String name, int remian, int minimum, String status) {
        this.id = -1;
        this.name = name;
        this.remian = remian;
        this.minimum = minimum;
        this.status = status;
    }
     public LowStockReport() {
        this(-1,"",0,0,"");
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRemian() {
        return remian;
    }

    public void setRemian(int remian) {
        this.remian = remian;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LowStockReport{" + "id=" + id + ", name=" + name + ", remian=" + remian + ", minimum=" + minimum + ", status=" + status + '}';
    }
    public static LowStockReport fromRS(ResultSet rs) {
        LowStockReport lsr = new LowStockReport();
        try {
            lsr.setId(rs.getInt("mat_id"));
            lsr.setName(rs.getString("mat_name"));
            lsr.setRemian(rs.getInt("Remain"));
            lsr.setMinimum(rs.getInt("minimum"));
            lsr.setStatus(rs.getString("Status"));

        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return lsr;
    }
  
}
