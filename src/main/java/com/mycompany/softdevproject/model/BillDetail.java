/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.BillDao;
import com.mycompany.databaseproject.dao.MeterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kunpo
 */
public class BillDetail {

    private int id;
    private int materialId;
    private String materialName;
    private float materialPrice;
    private int amount;
    private float total;
    private int billId;
    private Bill bill;
    private Meterial material;

    public BillDetail(int id, int materialId, String materialName, float materialPrice, int amount, float total, int billId) {
        this.id = id;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialPrice = materialPrice;
        this.amount = amount;
        this.total = total;
        this.billId = billId;
    }

    public BillDetail(int materialId, String materialName, float materialPrice, int amount, float total, int billId) {
        this.id = -1;
        this.materialId = materialId;
        this.materialName = materialName;
        this.materialPrice = materialPrice;
        this.amount = amount;
        this.total = total;
        this.billId = billId;
    }

    public BillDetail() {
        this.id = -1;
        this.materialId = 0;
        this.materialName = "";
        this.materialPrice = 0;
        this.amount = 0;
        this.total = 0;
        this.billId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public float getMaterialPrice() {
        return materialPrice;
    }

    public void setMaterialPrice(float materialPrice) {
        this.materialPrice = materialPrice;
        total = materialPrice * amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
        total = amount * materialPrice;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public Meterial getMaterial() {
        return material;
    }

    public void setMaterial(Meterial material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", materialId=" + materialId + ", materialName=" + materialName + ", materialPrice=" + materialPrice + ", amount=" + amount + ", total=" + total + ", billId=" + billId + ", bill=" + bill + ", material=" + material + '}';
    }



    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billdetail = new BillDetail();
        try {
            billdetail.setId(rs.getInt("bill_detail_id"));
            billdetail.setMaterialId(rs.getInt("mat_id"));
            billdetail.setMaterialName(rs.getString("material_name"));
            billdetail.setMaterialPrice(rs.getFloat("material_price"));
            billdetail.setAmount(rs.getInt("bill_detail_amount"));
            billdetail.setTotal(rs.getFloat("bill_detail_total"));
            billdetail.setBillId(rs.getInt("bill_id"));
            MeterialDao matDao = new MeterialDao();
            Meterial material = matDao.get(rs.getInt("mat_id"));
            //Popuulation
            billdetail.setMaterial(material);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billdetail;
    }
}
