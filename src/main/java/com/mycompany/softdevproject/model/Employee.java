/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Employee {

    private int id;
    private String name;
    private String login;
    private String password;
    private String gender;
    private int role;

    public Employee(int id, String name, String login, String password, String gender, int role) {
        this.id = id;
        this.name = name;
        this.login = login;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public Employee(String name, String login, String password, String gender, int role) {
        this.id = -1;
        this.name = name;
        this.login = login;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.login = "";
        this.password = "";
        this.gender = "";
        this.role = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", login=" + login + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }

    public static Employee fromRs(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setLogin(rs.getString("employee_login"));
            employee.setPassword(rs.getString("employee_password"));
            employee.setGender(rs.getString("employee_gender"));
            employee.setRole(rs.getInt("employee_role"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        return employee;
    }
}
