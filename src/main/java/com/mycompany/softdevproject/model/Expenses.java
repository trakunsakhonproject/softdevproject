/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.EmployeeDao;
import com.mycompany.databaseproject.dao.MemberDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anawi
 */
public class Expenses {

    private int id;
    private Timestamp date;
    private float total;
    private int employeeId;
    private Employee employee;
    private ArrayList<ExpensesDetail> expensesDetails = new ArrayList();

    public Expenses(int id, Timestamp date, float total, int employeeId) {
        this.id = id;
        this.date = date;
        this.total = total;
        this.employeeId = employeeId;
    }

    public Expenses(Timestamp date, float total, int employeeId) {
        this.id = 0;
        this.date = date;
        this.total = total;
        this.employeeId = employeeId;
    }

    public Expenses(float total, int employeeId) {
        this.id = 0;
        this.date = null;
        this.total = total;
        this.employeeId = employeeId;

    }

    public Expenses() {
        this.id = 0;
        this.date = null;
        this.total = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public ArrayList<ExpensesDetail> getExpensesDetails() {
        return expensesDetails;
    }

    public void setExpensesDetails(ArrayList<ExpensesDetail> expensesDetails) {
        this.expensesDetails = expensesDetails;
    }

    public void addExpensesDetail(String name, Float total) {
        ExpensesDetail e = new ExpensesDetail(name, total);
        expensesDetails.add(e);
        calculateTotal();
    }

    public void delExpensesDetail(ExpensesDetail expensesDetail) {
        expensesDetails.remove(expensesDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        Float total = 0.0f;
        for (ExpensesDetail ed : expensesDetails) {
            total += ed.getTotal();
        }
        this.total = total;
    }

    @Override
    public String toString() {
        return "Expenses{" + "id=" + id + ", date=" + date + ", total=" + total + ", employeeId=" + employeeId + ", employee=" + employee + ", expensesDetails=" + expensesDetails + '}';
    }

    public static Expenses fromRS(ResultSet rs) {
        Expenses expenses = new Expenses();
        try {
            expenses.setId(rs.getInt("expenses_id"));
            expenses.setDate(rs.getTimestamp("expenses_date"));
            expenses.setTotal(rs.getFloat("expenses_total"));
            expenses.setEmployeeId(rs.getInt("employee_id"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(expenses.getEmployeeId());
            expenses.setEmployee(employee);

            // Handle null employee case similarly if necessary
        } catch (SQLException ex) {
            Logger.getLogger(Expenses.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expenses;

    }

}
