/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softdevproject.model;

import com.mycompany.databaseproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class Summary {

    private int id;
    private Date date;
    private int employeeId;
    private Employee employee;
    private String employeeName;
    private Double employeeHourWage;
    private int workHour;
    private Double total;
    private ArrayList<CheckInOut> Checkinouts = new ArrayList();

    public Summary(int id, Date date, int employeeId, Employee employee, String employeeName, Double employeeHourWage, int workHour, Double total) {
        this.id = id;
        this.date = date;
        this.employeeId = employeeId;
        this.employee = employee;
        this.employeeName = employeeName;
        this.employeeHourWage = employeeHourWage;
        this.workHour = workHour;
        this.total = total;
    }

    public Summary(Date date, int employeeId, String employeeName, Double employeeHourWage, int workHour, Double total) {
        this.date = date;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeHourWage = employeeHourWage;
        this.workHour = workHour;
        this.total = total;
    }

    public Summary() {
        this.id = -1;
        this.date = null;
        this.employeeId = 0;
        this.employeeName = "";
        this.employeeHourWage = 0.00;
        this.workHour = 0;
        this.total = 0.00;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
//        this.employeeId = employee.getId();
    }

    public Double getEmployeeHourWage() {
        return employeeHourWage;
    }

    public void setEmployeeHourWage(Double employeeHourWage) {
        this.employeeHourWage = employeeHourWage;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ArrayList<CheckInOut> getCheckinouts() {
        return Checkinouts;
    }

    public void setCheckinouts(ArrayList<CheckInOut> Checkinouts) {
        this.Checkinouts = Checkinouts;
    }

    public int getWorkHour() {
        return workHour;
    }

    public void setWorkHour(int workHour) {
        this.workHour = workHour;
    }

    @Override
    public String toString() {
        return "Summary{" + "id=" + id + ", date=" + date + ", employeeId=" + employeeId + ", employee=" + employee + ", employeeName=" + employeeName + ", employeeHourWage=" + employeeHourWage + ", workHour=" + workHour + ", total=" + total + ", Checkinouts=" + Checkinouts + '}';
    }

  

    public static Summary fromRS(ResultSet rs) {
        Summary summary = new Summary();
        try {
            summary.setId(rs.getInt("ss_id"));
            summary.setDate(rs.getTimestamp("ss_date"));
            summary.setEmployeeName(rs.getString("employee_name"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee emp = employeeDao.get(rs.getInt("employee_id"));
            summary.setEmployee(emp);
            summary.setEmployeeHourWage(rs.getDouble("employee_hour_wage"));
            summary.setTotal(rs.getDouble("ss_total"));
            summary.setWorkHour(rs.getInt("ss_work_hour"));
        } catch (SQLException ex) {
            Logger.getLogger(Summary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return summary;
    }

}
