/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import com.mycompany.databaseproject.dao.MeterialDao;
import com.mycompany.databaseproject.dao.ProductDao;
import com.mycompany.databaseproject.dao.ReceiptDao;
import com.mycompany.softdevproject.model.CheckMat;
import com.mycompany.softdevproject.model.CheckMatDetail;
import com.mycompany.softdevproject.model.Meterial;
import com.mycompany.softdevproject.model.Product;
import com.mycompany.softdevproject.model.Receipt;
import com.mycompany.softdevproject.model.ReceiptDetail;
import com.mycompany.softdevproject.service.CheckMatService;
import com.mycompany.softdevproject.service.ReceiptService;
import java.util.List;

/**
 *
 * @author anawi
 */
public class TestCheckMatService {

    public static void main(String[] args) {
        CheckMatService cs = new CheckMatService();
        for (CheckMat checkMat : cs.getCheckMats()) {
            System.out.println(checkMat);
        }
        System.out.println(cs.getById(1));
        CheckMat c1 = new CheckMat(4);

        MeterialDao md = new MeterialDao();
        List<Meterial> materials = md.getAll();
        Meterial material0 = materials.get(0);
        Meterial material1 = materials.get(1);
        CheckMatDetail newCheckMat1 = new CheckMatDetail(material0.getName(), material0.getMinimum(), 2, material0.getId(), -1);
        c1.adddCheckMatDetail(newCheckMat1);
        CheckMatDetail newCheckMat2 = new CheckMatDetail(material1.getName(), material1.getMinimum(), 1, material1.getId(), -1);
        c1.adddCheckMatDetail(newCheckMat2);
        cs.addNew(c1,9);
        for (CheckMat checkMat : cs.getCheckMats()) {
            System.out.println(checkMat);
        }
    }
}
